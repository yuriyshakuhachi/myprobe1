import {Dom} from "./Dom";

const myDom = new Dom('Yuri', 'root');

const url = 'https://swapi.dev/api/films/';
let promiseFilms = fetch(url)
    .then(response => response.json())
    .then((films) => {
        for (let {episode_id, title, opening_crawl, characters} of films.results) {
            myDom.string = `Episode: ${episode_id} "${title}"`;
            let id = (myDom._id-1);
            for (let urlChar of characters) {
                let promiseChars = fetch(urlChar)
                    .then(response => response.json())
                    .then((char) => {
                        // myDom.string =`${char.name}`;
                        myDom.stringSetAfterId(`${char.name}`, id);
                    })
            }
            myDom.string = ` ${opening_crawl}`;
        }
    });







