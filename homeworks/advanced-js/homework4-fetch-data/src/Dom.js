export class Dom {
    constructor(name, idRoot) {
        this._name = name;
        this._idRoot = idRoot;
        this._id = 0;
    }

    /**
     * Вставляет строку после элемента с idRoot
     * и присваивает новому элементу  id++
     * @param stroka - строковое выражение
      */
    set string(stroka) {
        this._string = stroka;
        const root = document.getElementById(`${this._idRoot}`);
        let llast = document.createElement('li');
        llast.setAttribute("id",this._id);
        llast.innerHTML = stroka;
        root.append(llast);
        this._id++;
    }

    /**
     * Вставляет строку после элемента с id и
     * добавляет к id нового элемента префикс вложенности ".1"
     * @param stroka - строковое выражение
     * @param id - number
     */
    stringSetAfterId(stroka, id){
        this._string = stroka;
        let lLast = document.getElementById(`${id}`);
        const lNew = document.createElement('li');
        lNew.setAttribute("id",`${id}.1`);
        lNew.innerHTML = stroka;
        lLast.append(lNew);
    }
}


