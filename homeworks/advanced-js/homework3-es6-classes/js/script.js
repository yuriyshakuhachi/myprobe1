"use strict";

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    getName() {
        return `${this.name}`
    };

    getAge() {
        return `${this.age}`
    };

    getSalary() {
        return `${this.salary}`
    };

    setName(value) {
        this.name = value;
        return 'ok';
    };

    setAge(value) {
        this.age = value;
        return 'ok';
    };

    setSalary(value) {
        this.salary = value;
        return 'ok';
    };
}

class Programmer extends Employee {
    constructor(lang, name, age, salary) {
        super(name, age, salary);
        this.lang = lang;
    }
    getSalary() {
        return super.getSalary()*3;
    }
}

const employee1 = new Employee('Vasil_Vishivany', 30, 10000);
const programmer1 = new Programmer('JS,Pascal,Clipper,FoxPro', 'Yurii_Shulha', 49, 10000);
const programmer2 = new Programmer('JS,C++,Assembler', 'Sergey_Donchenko', 42, 20000);


console.log(programmer1);
console.log(programmer2);


