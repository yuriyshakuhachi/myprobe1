// Задание 6

const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

const newEmployee = {
    ...employee,
    age: 49,
    salary: 100000,
};
console.log(newEmployee);
