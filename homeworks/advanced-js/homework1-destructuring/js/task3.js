// Задание 3
const user1 = {
    name: "John",
    years: 30,
    // isAdmin: "yes",
};

const {
    name,
    years: age,
    isAdmin: isAdmin = false,
} = user1

alert(`name: ${name}`);
alert(`age: ${age}`);
alert(`isAdmin: ${isAdmin}`)

