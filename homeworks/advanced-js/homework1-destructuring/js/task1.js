//Задание 1
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
//вариант 1
// const concated = [...clients1, ...clients2];
//
// concated.forEach(function delDubl(item, i, arr) {
//     const indexdel = arr.indexOf(item, i + 1);
//     if (indexdel !== -1) {
//         arr.splice(indexdel, 1)
//     }
// })

//вариант 2
let concated = [...new Set([...clients1,...clients2])];
