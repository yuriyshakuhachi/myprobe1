##Теоретический вопрос:
Приведите пару примеров, когда уместно использовать в коде конструкцию try...catch.
##Теоретический ответ:
Обычно это узкие места при валидации например:
некорректный тип ожидаемого свойства,
или отсутствие свойства объекта, 
и переменная не определена. 
