/** создает объект и предлогает пользователю ввести свои имя и фамилию дату рождения
 *   считает количество полных лет
 * возвращает объект
 * @returns {{getLogin: (function(): string), firstName: string, lastName: string, getNames: getNames}}
 */
function createNewUser() {
    const newUser = {
        firstName: '',
        lastName: '',
        birthday: '',
        getNames: function () {
            this.firstName = prompt('введите имя:', '');
            this.lastName = prompt('введите фамилию:', '');
            this.birthday = prompt('введите дату рождения dd.mm.yyyy', '')
        },
        getLogin: function () {
            return (this.firstName.slice(0, 1) + this.lastName).toLowerCase();
        },
        getAge: function () {
            const yyyy_mm_dd = this.birthday.slice(6, 10) + '-' + this.birthday.slice(3, 5) + '-' + this.birthday.slice(0, 2);
            const birthDate = new Date(yyyy_mm_dd);
            const now = new Date();
            age = now.getFullYear() - birthDate.getFullYear();
            return age;
        },
        getPassword: function () {
            const yyyy_mm_dd = this.birthday.slice(6, 10) + '-' + this.birthday.slice(3, 5) + '-' + this.birthday.slice(0, 2);
            const birthDate = new Date(yyyy_mm_dd);
            const year = birthDate.getFullYear()
            return ((this.firstName.slice(0, 1)).toUpperCase() + this.lastName.toLowerCase() + year);
        }
    };
    newUser.getNames();
    console.log(newUser.getLogin());
    console.log(newUser.getAge());
    console.log(newUser.getPassword());
    return newUser
}

console.log(createNewUser());




