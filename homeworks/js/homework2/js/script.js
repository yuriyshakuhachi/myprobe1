let EndRange = '';
let Count5 = 0;
do {
    EndRange = prompt('Начало - 0, введите конец диапазона (целое число):', EndRange);
} while (!Number.isInteger(Number(EndRange)) || '' === (EndRange.trim())); // проверяем на валидность
EndRange = Number(EndRange);
if (Math.sign(EndRange) === 1 || 0) {
    for (let i = 0; i <= EndRange; i++) {
        if (i % 5 === 0 && i !== 0) {
            console.log(i);
            Count5++;
        }
    }
    // для отрицательных чисел
} else {
    for (let i = 0; i >= EndRange; i--) {
        if (i % 5 === 0 && i !== 0) {
            console.log(i);
            Count5++;
        }
    }
}

// ничего не нашли
if (Count5 === 0) {
    console.log('Sorry, no numbers');
}


