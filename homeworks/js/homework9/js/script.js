document.addEventListener("DOMContentLoaded", function (oldChild) {
    let persons = document.getElementById('tabs');
    let contents = document.getElementById('tabs-content');
    let content = [];
    let last = 0;
    for (let i = 0; contents.children.length !== 0; i++) {
        content[i] = contents.removeChild(contents.children.item(0));
    }
    contents.appendChild(content[0]);

    for (let i = 0; i < persons.children.length; i++) {
        persons.children.item(i).addEventListener("click", function () {
            showContent(persons.children.item(i), i);
        });
    }

    function showContent(obj, i) {
        persons.children.item(last).setAttribute('class', 'tabs-title');
        contents.removeChild(contents.children.item(0));
        last = i;
        contents.appendChild(content[i]);
        obj.setAttribute('class', 'tabs-title active');
    }

});
