let number1 = '';
let number2 = '';
let operation = '';

/**@number1, @number2 {number}
 * @operation {string}
 * @return {number}
 */
function Calc(number1, number2, operation) {
    switch (operation) {
        case '+':
            return number1 + number2;
        case '-':
            return number1 - number2;
        case '*':
            return number1 * number2;
        case '/':
            return number1 / number2;
    }
}
/**@number1, @number2 {sting}
 *
 * @return {boolean}
 */
function FalseDate(number1, number2) {
    if (number1.trim() === '' || number2.trim() === '') {
        return true;
    } else {
        return (Number(number1) !== Number(number1) || Number(number2) !== Number(number2));
    } // при проверке здесь используется свойство ( NaN !== NaN ) когда Number() не есть число
}

do {
    number1 = prompt('введите первое число:', number1);
    number2 = prompt('введите второе число:', number2);
    operation = prompt('введите операцию:', operation);
} while (FalseDate(number1, number2)); // проверяем на валидность

number1 = Number(number1);
number2 = Number(number2);


console.log(Calc(number1, number2, operation));


