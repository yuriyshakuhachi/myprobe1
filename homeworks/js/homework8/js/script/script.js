document.addEventListener("DOMContentLoaded", function () {
    let price = document.getElementById('price');
    price.addEventListener("focusout", readPrice);

    function readPrice() {
        const notcorrect = document.getElementById('notcorrect');
        if (notcorrect !== null) notcorrect.remove(); // убирам: Please enter correct price , если есть
        if (falseNum(price.value)) {
            price.style.backgroundColor = "red";
            price.insertAdjacentHTML('afterend', '<p id="notcorrect"><span class="notcorrect">Please enter correct price</span></p>');
            return 0
        }
        let span = document.getElementById('span');
        if (span !== null) span.remove();// убирам старый спан, если он есть
        const label = document.getElementById('label');
        label.insertAdjacentHTML('beforebegin', '<p id="span"><span class="prce" >Текущая цена:</span></p>');
        span = document.getElementById('span');
        span.innerText = "Текущая цена: " + price.value;
        span.insertAdjacentHTML('beforeend', `<button id="button" class="button">Х</button>`);
        const button = document.getElementById('button');
        button.addEventListener("click", clickButton);
        price.style.backgroundColor = "green";
    }

    function clickButton() {
        const span = document.getElementById('span');
        span.remove();
        price.value = null;
    }

    /**принимает строку
     * @return {boolean}
     * если преобразованная строка не есть число или число меньше 0
     */
    function falseNum(number) {
        if (number.trim() === '') {
            return true;
        } else {
            const n = Number(number);
            return (n !== n || n < 0);
        } // при проверке здесь используется свойство ( NaN !== NaN ) когда Number() не есть число
    }

});