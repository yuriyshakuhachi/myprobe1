"use strict";
document.addEventListener('DOMContentLoaded', function () {
    const books = [
        {
            author: "Скотт Бэккер",
            name: "Тьма, что приходит прежде",
            price: 70
        },
        {
            author: "Скотт Бэккер",
            name: "Воин-пророк",
        },
        {
            name: "Тысячекратная мысль",
            price: 70
        },
        {
            author: "Скотт Бэккер",
            name: "Нечестивый Консульт",
            price: 70
        },
        {
            author: "Дарья Донцова",
            name: "Детектив на диете",
            price: 40
        },
        {
            author: "Дарья Донцова",
            name: "Дед Снегур и Морозочка",
        }
    ];
    const showbooks = function (arg) {
        const root = document.getElementById('root');
        for (let {author, name, price} of arg){
            try {
                if (author === undefined ) {
                    throw new Error("отсутствует поле: author");
                }
                if (name === undefined) {
                    throw new Error("отсутствует поле: name");
                }
                if (price === undefined) {
                    throw new Error("отсутствует поле price");
                }
                let lilast = document.createElement('li');
                lilast.innerHTML = `${author} "${name}" ${price}грн.`;
                root.append(lilast);
            } catch (error) {
                console.log(`${error.name}: ${error.message}`);
            }
        }
    };
    showbooks(books);

});

