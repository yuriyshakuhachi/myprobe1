$(document).ready(function () {
    'use strict';

    /**
     * Вешает события для отображения контента для выбранного меню
     * @param tabs - строка с именем id родительского объекта содержащего пункты меню
     * @param contents  - строка с именем id родительского объекта содержащего контент
     * число html тегов ( ячеек ) в меню и контенте должно совпадать.
     */
    function menuTwoObj(tabs, contents) {
        let tabsO = document.getElementById(tabs);
        let contentsO = document.getElementById(contents);
        let content = [];
        let last = 0;

        for (let i = 0; contentsO.children.length !== 0; i++) {
            content[i] = contentsO.removeChild(contentsO.children.item(0));
        }

        let classNameTabActive = tabsO.children.item(0).getAttribute('class');
        let classNameTabPassive = tabsO.children.item(1).getAttribute('class');
        contentsO.appendChild(content[0]);

        for (let i = 0; i < tabsO.children.length; i++) {
            tabsO.children.item(i).addEventListener("click", function () {
                showContent(tabsO.children.item(i), i);
            });
        }

        function showContent(obj, i) {
            tabsO.children.item(last).setAttribute('class', classNameTabPassive);
            contentsO.removeChild(contentsO.children.item(0));
            last = i;
            contentsO.appendChild(content[i]);
            obj.setAttribute('class', classNameTabActive);
        }
    }

    /**
     * вешает событие для отображения дополнительного контента при нажатии кнопки "LoadMore"
     */
    function loads() {
        const contentO = document.getElementById('w1');
        const content = contentO.removeChild(contentO.children.item(0));
        $('.loadMore').click(function () {
            contentO.appendChild(content);
            const form = document.getElementById('loadMore');
            form.removeChild(form.children.item(0));
        });

    }

    menuTwoObj('tabs', 'contents');
    menuTwoObj('b53', 'b54');
    loads();

    let tabsO = document.getElementById('staffTables');
    let contentsO = document.getElementById('staffContent');
    let content = [];
    let last = 0;
    let j = 0; /* смещение */
    let l = contentsO.children.length; /*количество элементов контента*/
    let lenTabs = tabsO.children.length;
    /**
     * выбирает весь контент в массив
     */
    for (let i = 0; contentsO.children.length !== 0; i++) {
        content[i] = contentsO.removeChild(contentsO.children.item(0));
    }
    contentsO.appendChild(content[0]); /* отображает первый елемент массива*/

    function ij(i) {
        let r = i + j;
        if (r < 0) {
            return r + l
        } else {
            return r
        }
    }

    function ijl(i) {
        let r = i + j;
        if (r >= l) {
            return r - l
        } else {
            return r
        }
    }

    /**
     * вешает события на элементы карусели для отображения контента
     */

    function caruselClickMenu() {
        for (let i = 0; i < tabsO.children.length; i++) {
            tabsO.children.item(i).addEventListener("click", function () {
                showContent(tabsO.children.item(i), i, ij(i));
            });
        }
    }

    caruselClickMenu();

    /** отображает контент для выбранного элемента карусели
     *
     * @param obj   - выбранный объект элемента карусели
     * @param i - номер выбранного элемента в меню карусели
     * @param ij - номер в массиве для отображения контента
     */
    function showContent(obj, i, ij) {
        tabsO.children.item(last).style.top = '15px';
        contentsO.removeChild(contentsO.children.item(0));
        last = i;
        contentsO.appendChild(content[ij]);
        obj.style.top = '5px';
    }

    $('.buttonRight').click(function () {
        j--;/*сдвигаем отображение контента относительно меню*/
        if (j <= -l) {
            j = 0
        }
        console.log(j);
        /*меняем морды в меню*/
        for (let i = 0; i < tabsO.children.length; i++) {
            tabsO.children.item(i).children.item(1).children.item(0).src =
                content[ij(i)].children.item(1).children.item(1).children.item(0).src;
        }
    });

    $('.buttonLeft').click(function () {
        j++;/*сдвигаем отображение контента относительно меню*/
        debugger;
        if (j  >= l) {
            j = 0
        }
        console.log(j);
        // console.log(ij(i,j));
        /*меняем морды в меню*/
        for (let i = 0; i < tabsO.children.length; i++) {
            tabsO.children.item(i).children.item(1).children.item(0).src =
                content[ijl(i)].children.item(1).children.item(1).children.item(0).src;
            // console.log(ij(i, j));
            // debugger
        }

    });


    // menuCarusel('staffTables', 'staffContent');

    // $('.s1').click(function () {
    //     // debugger;
    //     console.log('aaaaaa');
    //     document.getElementById("b34i").src = "img/breaking%20news/Layer%2052@1X.png";
    //     document.getElementById('t1').innerText = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto cumque, harum magnam minima mollitia" +
    //         " nam natus quas quasi quos repudiandae saepe, tempora veritatis vero. Dolor itaque nam odit sequi voluptates." +
    //         " Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aperiam commodi corporis delectus" +
    //         " dignissimos est fugiat ipsum labore minus, molestiae mollitia non praesentium, quod quos reiciendis sequi" +
    //         " sint, tempore temporibus!";
    // });
    // $('.s2').click(function () {
    //     document.getElementById("b34i").src = "img/breaking%20news/Layer%2053@1X.png";
    //     document.getElementById('t1').innerText = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto cumque, harum magnam minima mollitia" +
    //         "  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aperiam commodi corporis delectus" +
    //         " nam natus quas quasi quos repudiandae saepe, tempora veritatis vero. Dolor itaque nam odit sequi voluptates. dignissimos est fugiat ipsum labore minus, molestiae mollitia non praesentium, quod quos reiciendis sequi" +
    //         " sint, tempore temporibus!";
    // });
    // $('.s3').click(function () {
    //     document.getElementById("b34i").src = "img/breaking%20news/Layer%2051@1X.png";
    //     document.getElementById('t1').innerText = "dignissimos est fugiat ipsum labore minus, molestiae mollitia non praesentium, quod quos reiciendis sequi" +
    //         " sint, tempore temporibus! ;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto cumque, harum magnam minima mollitia" +
    //         " nam natus quas quasi quos repudiandae saepe, tempora veritatis vero. Dolor itaque nam odit sequi voluptates." +
    //         " Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aperiam commodi corporis delectus"
    //
    // });
    // $('.s4').click(function () {
    //     document.getElementById("b34i").src = "img/breaking%20news/Layer%2050@1X.png";
    //     document.getElementById('t1').innerText = "Lorem         document.getElementById('t1').innerText = \"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto cumque, harum magnam minima mollitia ipsum dolor sit amet, consectetur adipisicing elit. Architecto cumque, harum magnam minima mollitia" +
    //         " nam natus quas quasi quos repudiandae saepe, tempora veritatis vero. Dolor itaque nam odit sequi voluptates." +
    //         " Lorem ipsum dolo corporis delectus" +
    //         " dignissimos est fugiat ipsum labore minus, molestiae mollitia non praesentium, quod quos reiciendis sequi" +
    //         " sint, tempore temporibus!";
    // });
    // $('.s5').click(function () {
    //     document.getElementById("b34i").src = "img/breaking%20news/Layer%2055@1X.png";
    //     document.getElementById('t1').innerText = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto cumque, harum magnam minima mollitia" +
    //         " nam natus quas quasi quos repudiandae saepe, dignissimos est fugiat ipsum labore minus, molestiae mollitia non praesentium, quod quos reiciendis  tempora veritatis vero. Dolor itaque nam odit sequi voluptates." +
    //         " Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aperiam commodi corporis delectus" +
    //         " sequi" +
    //         " sint, tempore temporibus!";
    // });
    // $('.s6').click(function () {
    //     document.getElementById("b34i").src = "img/breaking%20news/Layer%2056@1X.png";
    //     document.getElementById('t1').innerText = "dolor sit amet, consectetur adipisicing elit. Architecto cumque, harum magnam minima Lorem ipsum  mollitia" +
    //         " nam natus quas quasi quos repudiandae saepe, tempora veritatis vero. Dolor itaque nam odit sequi voluptates." +
    //         " Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aperiam commodi corporis delectus" +
    //         " dignissimos est fugiat ipsum labore minus, molestiae mollitia non praesentium, quod quos reiciendis sequi" +
    //         " sint, tempore temporibus!";
    // });

    // $('.b531').click(function () {
    //     document.getElementById('b54i1').src = "img/landing%20page/landing-page1.jpg";
    //     document.getElementById('b54i2').src = "img/graphic design/graphic-design2.jpg";
    //     document.getElementById('b54i3').src = "img/web%20design/web-design1.jpg";
    //     document.getElementById('b54i4').src = "img/wordpress/wordpress1.jpg";
    //     document.getElementById('b54i5').src = "img/graphic design/graphic-design5.jpg";
    //     document.getElementById('b54i6').src = "img/web%20design/web-design2.jpg";
    //     document.getElementById('b54i7').src = "img/breaking%20news/Layer%2047@1X.png";
    //     document.getElementById('b54i8').src = "img/landing%20page/landing-page2.jpg";
    //     document.getElementById('b54i9').src = "img/web%20design/web-design3.jpg";
    //     document.getElementById('b54i10').src = "img/wordpress/wordpress3.jpg";
    //     document.getElementById('b54i11').src = "img/graphic design/graphic-design10.jpg";
    //     document.getElementById('b54i12').src = "img/breaking%20news/Layer%2051@1X.png";
    // });
    // $('.b532').click(function () {
    //     document.getElementById('b54i1').src = "img/graphic design/graphic-design1.jpg";
    //     document.getElementById('b54i2').src = "img/graphic design/graphic-design5.jpg";
    //     document.getElementById('b54i3').src = "img/graphic design/graphic-design3.jpg";
    //     document.getElementById('b54i4').src = "img/graphic design/graphic-design4.jpg";
    //     document.getElementById('b54i5').src = "img/graphic design/graphic-design2.jpg";
    //     document.getElementById('b54i6').src = "img/graphic design/graphic-design6.jpg";
    //     document.getElementById('b54i7').src = "img/graphic design/graphic-design7.jpg";
    //     document.getElementById('b54i8').src = "img/graphic design/graphic-design8.jpg";
    //     document.getElementById('b54i9').src = "img/graphic design/graphic-design9.jpg";
    //     document.getElementById('b54i10').src = "img/graphic design/graphic-design10.jpg";
    //     document.getElementById('b54i11').src = "img/graphic design/graphic-design11.jpg";
    //     document.getElementById('b54i12').src = "img/graphic design/graphic-design12.jpg";
    // });
    // $('.b533').click(function () {
    //     document.getElementById('b54i1').src = "img/web%20design/web-design1.jpg";
    //     document.getElementById('b54i2').src = "img/web%20design/web-design2.jpg";
    //     document.getElementById('b54i3').src = "img/web%20design/web-design3.jpg";
    //     document.getElementById('b54i4').src = "img/web%20design/web-design4.jpg";
    //     document.getElementById('b54i5').src = "img/web%20design/web-design5.jpg";
    //     document.getElementById('b54i6').src = "img/web%20design/web-design2.jpg";
    //     document.getElementById('b54i7').src = "img/web%20design/web-design6.jpg";
    //     document.getElementById('b54i8').src = "img/web%20design/web-design7.jpg";
    //     document.getElementById('b54i9').src = "img/web%20design/web-design3.jpg";
    //     document.getElementById('b54i10').src = "img/web%20design/web-design5.jpg";
    //     document.getElementById('b54i11').src = "img/web%20design/web-design1.jpg";
    //     document.getElementById('b54i12').src = "img/web%20design/web-design6.jpg";
    // });
    // $('.b534').click(function () {
    //     document.getElementById('b54i1').src = "img/landing%20page/landing-page1.jpg";
    //     document.getElementById('b54i2').src = "img/landing%20page/landing-page2.jpg";
    //     document.getElementById('b54i3').src = "img/landing%20page/landing-page3.jpg";
    //     document.getElementById('b54i4').src = "img/landing%20page/landing-page4.jpg";
    //     document.getElementById('b54i5').src = "img/landing%20page/landing-page5.jpg";
    //     document.getElementById('b54i6').src = "img/landing%20page/landing-page6.jpg";
    //     document.getElementById('b54i7').src = "img/landing%20page/landing-page7.jpg";
    //     document.getElementById('b54i8').src = "img/landing%20page/landing-page2.jpg";
    //     document.getElementById('b54i9').src = "img/landing%20page/landing-page4.jpg";
    //     document.getElementById('b54i10').src = "img/landing%20page/landing-page3.jpg";
    //     document.getElementById('b54i11').src = "img/landing%20page/landing-page5.jpg";
    //     document.getElementById('b54i12').src = "img/landing%20page/landing-page6.jpg";
    // });
    // $('.b535').click(function () {
    //     document.getElementById('b54i1').src = "img/wordpress/wordpress1.jpg";
    //     document.getElementById('b54i2').src = "img/wordpress/wordpress2.jpg";
    //     document.getElementById('b54i3').src = "img/wordpress/wordpress3.jpg";
    //     document.getElementById('b54i4').src = "img/wordpress/wordpress4.jpg";
    //     document.getElementById('b54i5').src = "img/wordpress/wordpress5.jpg";
    //     document.getElementById('b54i6').src = "img/wordpress/wordpress6.jpg";
    //     document.getElementById('b54i7').src = "img/wordpress/wordpress7.jpg";
    //     document.getElementById('b54i8').src = "img/wordpress/wordpress8.jpg";
    //     document.getElementById('b54i9').src = "img/wordpress/wordpress9.jpg";
    //     document.getElementById('b54i10').src = "img/wordpress/wordpress10.jpg";
    //     document.getElementById('b54i11').src = "img/wordpress/wordpress3.jpg";
    //     document.getElementById('b54i12').src = "img/wordpress/wordpress5.jpg";
    // });

    let staff = [];
    staff[0] = {
        name: 'SHAPOCHNICOVA ANASTASIYA ',
        position: 'WEB Designer',
        img: 'img/carusel/Image1@1X.png'
    };
    staff[1] = {
        name: 'PETROV ALEX',
        position: 'FRONT end Programmer',
        img: 'img/carusel/Image2@1X.png'
    };
    staff[2] = {
        name: 'HASAN ALI',
        position: 'UX Designer',
        img: 'img/carusel/Image3@1X.png'
    };
    staff[3] = {
        name: 'BELOBROVA EVGENIYA',
        position: 'IT Consultant',
        img: 'img/carusel/Image4@1X.png'
    };
    staff[4] = {
        name: 'SVIRIDOV SERGEY',
        position: 'Cloud Architect',
        img: 'img/carusel/Image5@1X.png'
    };
    staff[5] = {
        name: 'DEREVYANKO MARIYA',
        position: 'Computer forensic investigator',
        img: 'img/carusel/Image6@1X.png'
    };
    staff[6] = {
        name: 'KRASNAYA YULIYA',
        position: 'Health IT specialist',
        img: 'img/carusel/Image7@1X.png'
    };
    staff[7] = {
        name: 'SEBASTIANOV ANDREY',
        position: 'Software engineer',
        img: 'img/carusel/Image8@1X.png'
    };

    // i = 0;
    // $('.face1').click(function () {
    //     let el = document.querySelector('#face1');
    //     el.style.top = '5px';
    //     el = document.querySelector('#face2');
    //     el.style.top = '15px';
    //     el = document.querySelector('#face3');
    //     el.style.top = '15px';
    //     el = document.querySelector('#face4');
    //     el.style.top = '15px';
    //     document.getElementById('b76img').src = staff[i].img;
    //     document.getElementById('b75t0').innerText = staff[i].name;
    //     document.getElementById('b75t1').innerText = staff[i].position;
    //
    // });
    // $('.face2').click(function () {
    //     let el = document.querySelector('#face1');
    //     el.style.top = '15px';
    //     el = document.querySelector('#face2');
    //     el.style.top = '5px';
    //     el = document.querySelector('#face3');
    //     el.style.top = '15px';
    //     el = document.querySelector('#face4');
    //     el.style.top = '15px';
    //     document.getElementById('b76img').src = staff[i + 1].img;
    //     document.getElementById('b75t0').innerText = staff[i + 1].name;
    //      document.getElementById('b75t1').innerText = staff[i + 1].position;
    // });
    // $('.face3').click(function () {
    //     let el = document.querySelector('#face1');
    //     el.style.top = '15px';
    //     el = document.querySelector('#face2');
    //     el.style.top = '15px';
    //     el = document.querySelector('#face3');
    //     el.style.top = '5px';
    //     el = document.querySelector('#face4');
    //     el.style.top = '15px';
    //     document.getElementById('b76img').src = staff[i + 2].img;
    //     document.getElementById('b75t0').innerText = staff[i + 2].name;
    //     document.getElementById('b75t1').innerText = staff[i + 2].position;
    // });
    // $('.face4').click(function () {
    //     let el = document.querySelector('#face1');
    //     el.style.top = '15px';
    //     el = document.querySelector('#face2');
    //     el.style.top = '15px';
    //     el = document.querySelector('#face3');
    //     el.style.top = '15px';
    //     el = document.querySelector('#face4');
    //     el.style.top = '5px';
    //     document.getElementById('b76img').src = staff[i + 3].img;
    //     document.getElementById('b75t0').innerText = staff[i + 3].name;
    //     document.getElementById('b75t1').innerText = staff[i + 3].position;
    //
    // });


    // let i = 1;
    // let el = document.querySelector('#carusel');
    // for (let span of el.querySelectorAll('span.image1')) {
    //     span.style.position = 'relative';
    //     span.insertAdjacentHTML('beforeend', `<span style="position:absolute;left:0;top:0">${i}</span>`);
    //     i++;
    // }


});
