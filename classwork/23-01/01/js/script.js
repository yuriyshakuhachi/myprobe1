let b = 4;


console.log('Is integer -->', isInteger(10));


function power(x) {
    return x * x;
}

function doSomethingWrong(y) {
    return b * y;
}

/**
 *
 **/
function product(x = 1, y = 1, z = 1) {
    return x * y * z;
}

/**
 * @desc Checks if passed argument is Integer
 * @param {*} number
 * @return {Boolean}
 **/
function isInteger(number) {
    return Number.isInteger(number);
}


function minNumbers(x = 0, y = 0, z = 0) {
    x = +x;
    y = +y;
    z = +z;

    if (x <= y && x <= z) {
        return x;
    }

    if (y <= z) {
        return y;
    }

    return z;
}
