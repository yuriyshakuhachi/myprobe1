/**
 * @desc Функция которая считает высоту здания
 * @param {Number}
 * @param {Number}
 * @param {Number}
 * @return {Number}
 **/
function calcHeight(fundament = 10, tsokol = 5, krusha = 19) {
    return fundament + tsokol + krusha;
}

/**
 * @desc Площадь здания
 * @param {Number}
 * @param {Number}
 * @return {Number}
 **/
function calcSquare(a, b) {
    return (a * b);
}