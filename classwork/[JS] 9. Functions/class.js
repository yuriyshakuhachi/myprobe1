/* ЗАДАНИЕ - 3
* Написать функцию, которая будет суммировать ВСЕ числа, которые будут переданы ей в качестве аргументов.
* */
  // const sumNumber = function (...numbs) {
  //     let sum = 0;
  //
  //     for (let i = 0; i < numbs.length; i++) {
  //         sum = sum + numbs[i]
  //     }
  //       return sum
  // }
  //   const sumNumber = (...numbs) => {
  //       let sum = 0;
  //
  //       for (let i = 0; i < numbs.length; i++) {
  //           sum = sum + numbs[i]
  //       }
  //       return sum
  //   }
  // let result = sumNumber()
/* ЗАДАНИЕ - 4
* Написать СТРЕЛОЧНУЮ функцию, которая выводит переданное ей аргументом сообщение, указанное количество раз.
* Принимает два аргумента - само сообщение и число сколько раз его показать.
* Если первый аргумент(сообщение) не передан - ПО УМОЛЧАНИЮ присвоить этому аргументу - "Empty message"
* Если второй аргумент(количество раз) не передан - ПО УМОЛЧАНИЮ присвоить этому аргументу значение 1.
* */
//
// const userFunction = (UserMessage = 'Empty message', counter = 1) => {
//     for (let i = 0; i < counter; i++) {
//         console.log(UserMessage);
//     }
// }
//     userFunction(undefined, 5);
/* ЗАДАНИЕ - 5
* Написать СТРЕЛОЧНУЮ функцию, которая возвращает максимальный переданый ей аргумент.
* Т.е. функции может быть передано потенциально бесконечное количество аргументов(чисел),
* вернуть нужно самый большой из них.
* */

// const maxNumb1 = (...numbs) => {
//     let maxNumb = Math.max(...numbs);
//     return maxNumb;
// }
//
// const result2 = maxNumb1(1,2,7,24,195,183);

//
// const maxNumb2 = (...numbs) => {
//     if (numbs.length === 0){
//         return "You didn't enter numbers."
//     }
//     let maxNumb = Number.NEGATIVE_INFINITY;
//     for (let i of numbs) {
//         if (i > maxNumb) {
//             maxNumb = i;
//         }
//     }
//     return maxNumb;
// }
//
// const result3 = maxNumb2(-123, -54, 684, -93, 0, -684);


// const fruitShop = {
//     admin: {
//       secretCode: Math.random()*100,
//     },
//     items: {
//         banana: 5,
//         orange: 1,
//         apple: 2,
//         pineapple: 0,
//         wetermelon: 7,
//         kivi: 8,
//         lemon: 4,
//         melon: 0
//     },
//     // functions: {
//       getFruit(fruit, value) {
//         if (fruit.toLowerCase() in this.items) {
//             if (this.items[fruit] < value) {
//                 console.error("no such amount of fruit")
//             }
//             else {
//                 this.items[fruit.toLowerCase()] -= value;
//             }
//         }
//         else {console.error('no such fruts in shop')}
//          },
//       addFruit(fruit, amountOfItems, code) {
//         if (this.admin.secretCode === code) {
//             if (fruit in this.items) {
//                 return this.items[fruit] += amountOfItems;
//             }
//
//                 return  this.items[fruit] = amountOfItems;
//             }
//         console.error('Invalid code')
//         }
//     // }
// }
// fruitShop.addFruit('cherry', 10, 123);
// fruitShop.addFruit('orange', 9, 123);
// fruitShop.functions.getFruit.call(fruitShop, 'Banana', 3);
// fruitShop.functions.getFruit('Banana', 3);












const user = function (name, age) {
    const secretCode = 1234;
    return {
        userName: name,
        userAge: age,
        cashAmount: 1000,
        getCash(count, secret) {
            if (secret === secretCode) {
                this.cashAmount -= count;
                return count;
            } else {
                console.error('Invalid pin code');
            }
        }
    }
}

const u1 = user('Ivan', 28);
