/**@str {sting}
 * возвращает false если в строке аргумент не есть number
 * @return {boolean}
 */
function validnumber(str) {
    const strv = Number(str);
    if (str.trim() === '') {
        return false;
    } else {
        return !(strv !== strv);
    } // при проверке здесь используется свойство ( NaN !== NaN ) когда Number() не есть число
}

/**
 * выводит в консоль и в документ
 * @arg1
 */
function writeall(arg1) {
    document.write(arg1);
    console.log(arg1);
}