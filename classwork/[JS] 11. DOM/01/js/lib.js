const getObjectLength = obj => {
    if (typeof obj !== 'object') {
        return 0;
    }

    return Object.keys(obj).length;
};



function Humanoid() {

    this.name = 'Sergey';

}

function cloneObject(o) {
    const newObject = {};

    for(let key in o) {
        if (typeof o[key] === 'object') {
            newObject[key] = cloneObject(o[key]);
        } else {
            newObject[key] = o[key];
        }
    }

    return newObject;
}

/**
 * @return {String}
 **/
function randomRGB() {
    return `rgb(${Math.random()*255},${Math.random()*255},${Math.random()*255})`;
}

/**
 * @desc Do something
 * @return {Number}
 **/
function drawBackground() {
    return setTimeout(() => {

        document.body.style = `background-color: ${randomRGB()}`;

        drawBackground();
    }, 2000);
}


function createElement(element) {
    return document.createElement(element);
}
