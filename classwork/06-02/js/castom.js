const customDOM = {
    getALLElements: () => document.getElementsByTagName('*'),
    createElement: function (tagName) {
        return document.createElement(tagName);

    },
    append: function (parent, child) {
        return parent.appendChild(child)
    }
}